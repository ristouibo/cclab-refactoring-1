package ee.ignite.codelab.refactor.c05;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class EnglishIntegerTest {

    EnglishInteger app = new EnglishInteger();

    @Test
    public void  zero_two_seven_nine() throws Exception {
        test( 0 ).is( "zero" );
        test( 2 ).is( "two" );
        test( 7 ).is( "seven" );
        test( 9 ).is( "nine" );
    }

    @Test
    public void ten_eleven_twelve_thirteen_fifteen_eighteen() throws Exception {
        test( 10 ).is( "ten" );
        test( 11 ).is( "eleven" );
        test( 12 ).is( "twelve" );
        test( 13 ).is( "thirteen" );
        test( 15 ).is( "fifteen" );
        test( 18 ).is( "eighteen" );
    }

    @Test
    public void twenty() throws Exception {
        test( 20 ).is( "twenty" );
    }

    @Test
    public void thirty() throws Exception {
        test( 30 ).is( "thirty" );
    }

    @Test
    public void thirty_one() throws Exception {
        test( 31 ).is( "thirty-one" );
    }

    @Test
    public void forty() throws Exception {
        test( 40 ).is( "forty" );
    }

    @Test
    public void forty_nine() throws Exception {
        test( 49 ).is( "forty-nine" );
    }

    @Test
    public void fifty() throws Exception {
        test( 50 ).is( "fifty" );
    }

    @Test
    public void eighty_one() throws Exception {
        test( 81 ).is( "eighty-one" );
    }

    @Test
    public void ninety_nine() throws Exception {
        test( 99 ).is( "ninety-nine" );
    }

    @Test
    public void one_hundred() throws Exception {
        test( 100 ).is( "one hundred" );
    }

    @Test
    public void eight_hundred_and_seventy_nine() throws Exception {
        test( 879 ).is( "eight hundred and seventy-nine" );
    }

    @Test
    public void two_thousand_nine_hundred_and_fifty() throws Exception {
        test( 2950 ).is( "two thousand, nine hundred and fifty" );
    }

    @Test
    public void thirty_six_thousand_one_hundred_and_eleven() throws Exception {
        test( 36111 ).is( "thirty-six thousand, one hundred and eleven" );
    }

    @Test
    public void seven_hundred_and_ninety_one_thousand_one_hundred_and_ninety() {
        test( 791190 ).is( "seven hundred and ninety-one thousand, one hundred and ninety" );
    }

    @Test
    public void three_hundred_and_sixty_million_six_hundred_and_five() throws Exception {
        test( 360000605 ).is( "three hundred and sixty million, six hundred and five" );
    }

    @Test
    public void one_billion_one() throws Exception {
        test( 1000000001 ).is( "one billion, one" );
    }

    @Test
    public void two_billion_one_hundred_and_two_million_seven_hundred_thousand_nine_hundred_and_ninety_eight() throws Exception {
        test( 2102700998 ).is( "two billion, one hundred and two million, seven hundred thousand, nine hundred and ninety-eight" );
    }





    /*
       SUPPORT TEST METHODS
     */

    private Assertion test( Integer integer ) {
        return new Assertion( integer );
    }

    private class Assertion {

        private final Integer suppliedIntegerValue;

        Assertion(Integer value) {
            suppliedIntegerValue = value;
        }

        void is( String expectedEnglishText ) {
            assertEquals( expectedEnglishText, app.toEnglish( suppliedIntegerValue ) );
        }
    }


}