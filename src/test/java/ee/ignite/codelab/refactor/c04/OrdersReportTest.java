package ee.ignite.codelab.refactor.c04;

import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;

import static org.mockito.Mockito.mock;

public class OrdersReportTest {

    private OrdersReport report;

    Connection fakeConnection = mock( Connection.class );

    @Before
    public void init() {
        report = new OrdersReport();
        report.setConnection( fakeConnection );
    }

    @Test
    public void twoSampleOrders_getPrintedCorrectly() throws Exception {

        //TODO: set up test data for asserting that orders get printed correctly
        // mock statement creation & Statement object
        // mock query execution & ResultSet object
        // mock resultset to return different rows upon consecutive calls,
        // mock resultset to return different number, name, quantity, price values for differnt rows
        // redirect System.out to write to a data (eg string) buffer

        // run the method under test, assert the expected result matches

        /* create sample data to match this sample expected printout:
        *
        * Order number: 100
        * 1. Pencils 10 2.99
        * 2. Erasers 5 6.50
        * 3. Paper 2 1.00
        * Order number: 101
        * 1. Cartridge 2 1.99
        * 2. Laser Printer 1 199.00
        *
        * */


    }

}