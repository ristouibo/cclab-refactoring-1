package ee.ignite.codelab.refactor.c02;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class VotingMachineTest {

    VotingMachine machine;

    @Before
    public void init() {
        machine = new VotingMachine( 0 );
    }


    @Test
    public void downVote_reducesScore_byOne() {
        machine.changeVote( "", "Down" );
        assertScoreIs( -1 );
    }

    @Test
    public void upVote_increasesScore_byOne() {
        machine.changeVote( "", "Up" );
        assertScoreIs( 1 );
    }

    @Test
    public void changeVote_downToUp_increasesScore_byTwo() {
        machine.changeVote( "Down", "Up" );
        assertScoreIs( 2 );
    }

    @Test
    public void changeVote_upToDown_reducesScore_byTwo() {
        machine.changeVote( "Up", "Down" );
        assertScoreIs( -2 );
    }

    @Test
    public void removing_downVote_increasesScore_byOne() {
        machine.changeVote( "Down", "" );
        assertScoreIs( 1 );
    }

    @Test
    public void removing_upVote_reducesScore_ByOne() {
        machine.changeVote( "Up", "" );
        assertScoreIs( -1 );
        assertThat( machine.getScore(), is(-1) );
    }


    private void assertScoreIs( int expectedScore ) {
        assertThat( machine.getScore(), is(expectedScore) );
    }
}