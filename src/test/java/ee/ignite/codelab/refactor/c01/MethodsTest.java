package ee.ignite.codelab.refactor.c01;

import org.junit.Test;

import static org.junit.Assert.*;

public class MethodsTest {

    Methods methods = new Methods();

    @Test
    public void getCount_returnsSetCount() throws Exception {
        methods.count = 10;

        assertEquals( 10, methods.getCount() );
    }

}