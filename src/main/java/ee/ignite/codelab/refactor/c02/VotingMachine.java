package ee.ignite.codelab.refactor.c02;

public class VotingMachine {

    private int score;

    public VotingMachine(int initialScore) {
        this.score = initialScore;
    }

    public Integer getScore() {
        return score;
    }

    /* TODO: refactor - remove repetition and do one thing at a time */
    public void changeVote(String oldVote, String newVote) {
        if (!newVote.equals(oldVote)) {
            if ("Up".equals(newVote)) {
                score += ("Down".equals(oldVote) ? 2 : 1);
            } else if ("Down".equals(newVote)) {
                score -= ("Up".equals(oldVote) ? 2 : 1);
            } else if ("".equals(newVote)) {
                score += ("Up".equals(oldVote) ? -1 : 1);
            }
        }
    }

}
