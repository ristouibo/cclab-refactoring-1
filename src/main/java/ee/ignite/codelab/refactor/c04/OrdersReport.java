package ee.ignite.codelab.refactor.c04;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.MessageFormat;
import java.util.Date;

public class OrdersReport {

    private Connection conn;



    /* TODO: refactor - turn method's business logic into explicit code structure */
    public void printOrdersStartingFrom(Date date) throws Exception {
        String sql = "SELECT id, number, name, quantity, price "
                + "FROM orders o "
                + "LEFT JOIN order_items oi on oi.order_id = o.id "
                + "order by o.number";
        try (Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(sql) )  {
            String previousOrderNumber = null; int counter = 1;
            while (rs.next()) {
                String number = rs.getString("number");
                if (!number.equals(previousOrderNumber)) {
                    System.out.println("Order number: " + number);
                    previousOrderNumber = number; counter = 1;
                }
                String line = MessageFormat.format("{0}. {1} {2} {3}",
                        counter++, rs.getString("name"), rs.getString("quantity"),
                        rs.getString("price"));
                System.out.println(line);
            }
        }
    }



    protected void setConnection( Connection connection ) {
        this.conn = connection;
    }
}
