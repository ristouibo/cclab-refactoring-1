package ee.ignite.codelab.refactor.c04.model;

public class OrderItem {
    private String name;
    private String price;
    private String quantity;

    public OrderItem( String name, String price, String quantity ) {
        setName( name );
        setPrice( price );
        setQuantity( quantity );
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

}
