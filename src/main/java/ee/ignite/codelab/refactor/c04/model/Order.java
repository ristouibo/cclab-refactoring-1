package ee.ignite.codelab.refactor.c04.model;

import java.util.ArrayList;
import java.util.List;

public class Order {

    private String number;
    private List<OrderItem> items;

    public Order(String number) {
        setNumber( number );
    }

    public void setNumber( String number ) {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }

    public void addItem( OrderItem item ) {
        items.add( item );
    }

    public List<OrderItem> getItems() {
        return new ArrayList<>(items);
    }

}
