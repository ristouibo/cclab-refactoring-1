package ee.ignite.codelab.refactor.c05;

public class EnglishInteger {

    public static void main(String[] args) {

        /* TODO: refactor - improve structure and readability */
        if(args.length<1) System.out.println("Usage: EnglishInteger <integer value>");
        else if (args.length==1) try
        {
        System.out.println(   "In english: "+new EnglishInteger().toEnglish(Integer.parseInt(args[0])));
        } catch (Exception e) {
        System.out.println("Error: "+e.getMessage() );
        System.out.println("Usage: EnglishInteger <integer value>");}
        else System.out.println("Usage: EnglishInteger <integer value>");

    }

    /* TODO #1: implement - method to return english representation of given Integer x */
    /* TODO #2: apply step-by-step refactorings, to incrementally clean implementation code */
    public String toEnglish( Integer x ) {

        return "No idea, since I'm not implemented :(";

    }


}
