package ee.ignite.codelab.refactor.c01;

//minimal Session class, just so compiling works
public class Session {

    public String getCurrentUserName() {
        return "username";
    }

    public String getStatus() {
        return "status";
    }
}
