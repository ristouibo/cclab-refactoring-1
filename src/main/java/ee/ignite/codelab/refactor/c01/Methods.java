package ee.ignite.codelab.refactor.c01;

import java.util.ArrayList;
import java.util.List;

public class Methods {


    /* TODO: refactor - make the code say what it actually does */
    protected int count;

    public int getCount() {
        return count++;
    }


    /* TODO: refactor - make the code say what it actually does */
    protected List<Order> orders = new ArrayList<>();

    public void findFilledOrders(List<Order> o) {
        for (Order order : this.orders) {
            if( order.isFilled() ) {
                o.add(order);
            }
        }
    }


    /* TODO: refactor - simplify and make the code say what it actually does */
    public boolean access(Session s) {
        return  ((s.getCurrentUserName().equals("Admin")
        || s.getCurrentUserName().equals("Administrator"))
        && (s.getStatus().equals("preferredStatusX")
        || s.getStatus().equals("preferredStatusY")));
    }

}
