package ee.ignite.codelab.refactor.c03;


public class StringCalculator {

    /* TODO: refactor - remove repetition and do one thing at a time */

    public int add(String s) {
        if (s.isEmpty()) {
            return 0;
        }
        if (s.startsWith("//")) {
            int retVal = 0; //the value to be returned
            for (String n : s.substring(4).split("[\n"+ s.charAt(2) +"]")) {
                retVal += Integer.parseInt(n);
            }
            return retVal;
        } else {
            int r = 0; // result
            for (String n : s.split("[\n,]")) {
                r += Integer.parseInt(n);
            }
            return r;
        }
    }

}